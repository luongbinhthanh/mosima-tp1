package princ;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Random;

import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;


public class AnalyseWeka {
	public static void main (String[] args) throws Exception{
		BufferedReader buf = null;
		try {
			buf = new BufferedReader(new FileReader("ressources/learningBase/weka4.arff"));
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		Instances train = new Instances(buf);
		train.setClassIndex(train.numAttributes()-1);
		J48 dt = new J48();
		dt.buildClassifier(train);
		
		Evaluation eval = new Evaluation(train);
		eval.crossValidateModel(dt, train, 10, new Random(1));
		System.out.println(eval.toSummaryString("\n Results \n", true));
		System.out.println(eval.fMeasure(1)+" ; "+eval.precision(1)+" ; "+eval.recall(1));
//		System.out.println("Working Directory = " +
//	              System.getProperty("user.dir"));
//		DataSource source;
//		try {
//			source = new DataSource("ressources/learningBase/weka1.arff");
//			
//			Instances data = source.getDataSet();
//			
//			if (data.classIndex() == -1)
//				   data.setClassIndex(data.numAttributes() - 1);
//			String[] options = new String[1];
//			 options[0] = "-U";            // unpruned tree
//			 J48 tree = new J48();         // new instance of tree
//			 tree.setOptions(options);     // set the options
//			 tree.buildClassifier(data);   // build classifier
//			 System.out.println(tree.toString());
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
}
