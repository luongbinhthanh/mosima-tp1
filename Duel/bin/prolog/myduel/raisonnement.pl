use_module(library(jpl)).

explore_points2(OffSize,DefSize):-
	OffSize>=2*DefSize. /* true if search for defensive */

being_attacked2(Time):-
	Time<10.

areaCovered2(Radius,Size,MapWidth):-
	2*pi*Radius*Size<0.6*MapWidth.

inGoodHealth2(life):-
	life>3.

shotImpact2(Probability):-
	Probability>0.1.

/* DECISIONS */

explore2(Time,Size,Radius,MapWidth):-
	not(being_attacked2(Time)),
	areaCovered2(Radius,Size,MapWidth),
	jpl_call('sma.actionsBehaviours.MyBehavior',executeExplore,[],@(void)).

hunt2(Life,Time,OffSize,DefSize,Radius,MapWidth,EnemyInSight):-
	not(being_attacked2(Time)),
	not(areaCovered2(Radius,OffSize,MapWidth)),
	not(areaCovered2(Radius,DefSize,MapWidth)),
    jpl_call('sma.actionsBehaviours.MyBehavior',executeHunt,[],@(void));
	inGoodHealth2(Life),
	being_attacked2(Time),
	not(EnemyInSight),
	jpl_call('sma.actionsBehaviours.MyBehavior',executeHunt,[],@(void)).

toOpenFire2(EnemyInSight,P):-
	shotImpact2(P),
	EnemyInSight.

attack2(EnemyInSight):-
	EnemyInSight,
	jpl_call('sma.actionsBehaviours.MyBehavior',executeAttack,[],@(void)).


/*retreat(Life,Time):-  */
/*	not(inGoodHealth(Life)), */
/*	being_attacked(Time),  */
/*	jpl_call('sma.actionsBehaviours.MyBehavior',executeRetreat,[],@(void)). */
